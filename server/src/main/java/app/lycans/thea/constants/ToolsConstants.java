package app.lycans.thea.constants;

public class ToolsConstants {
    public static final String NAME = "name";
    public static final String TOOL = "Tool";
    public static final String TOOL_GROUP = "Tool-Group";
    public static final String SERVER_VALUE = "server-value";
    public static final String DISPLAY_NAME = "displayName";
    public static final String IS_DEFAULT = "isDefault";
    public static final String COMPONENT = "component";



    //GROUP_NAMES
    public static final String GROUP_STRING_UTILS = "String-Utils";



}
