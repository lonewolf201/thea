package app.lycans.thea.cache;

import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.Jedis;


public class StringRedisCache {
    private volatile static StringRedisCache stringRedisCache;
    private Jedis jedis;
    private StringRedisCache(String url, int port){
        jedis = new Jedis(new HostAndPort(url,port));
    }

    public static StringRedisCache getInstance(){
        if(stringRedisCache==null){
            stringRedisCache = new StringRedisCache("localhost",6379);
        }
        return stringRedisCache;
    }

    public void setString(String key,String value){
        jedis.set(key,value);
    }


    public String getString(String key){
        return jedis.get(key);
    }

}
