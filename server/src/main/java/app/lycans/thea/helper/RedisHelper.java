package app.lycans.thea.helper;

import redis.embedded.RedisServer;

import java.io.IOException;

public class RedisHelper {
    private static volatile RedisHelper redisHelper;
    private RedisServer redisServer;



    private RedisHelper(int port){
        try {
            this.redisServer = new RedisServer(port);
        }catch (IOException io){
            throw new RuntimeException(io);
        }
    }

    public static RedisHelper getInstance() throws IOException {
        if(redisHelper==null){
            redisHelper = new RedisHelper(6379);
        }
        return redisHelper;
    }

    public void startRedis(){
        redisServer.start();
    }

    public void stopRedis(){
        redisServer.stop();
    }

    public boolean isActive(){
        return redisServer.isActive();
    }



}
