![ok](https://gitlab.com/lonewolf201/lycan-apps/-/raw/main/Thea/logo.png?inline=false)

**T**he **H**elpful **E**legant **A**colyte 

# Description

One App that has it all. One stop solution for all you development needs.<br />
Check out the repo and releases.<br />
Devs can add their own tools to the collection too.<br />

# Build Thea from source
this is app is still in ALPHA.

Pre-req to build this app are:
- Java 8
- Maven
- Node
- Ember js

Optional (Yet recommended):
- Intellij Idea
- VS Code

This app has a client and a server code (for scalability when the app requires complex backend functions).

# Steps for Building:

## Step 1:
    Clone this repo.
## Step 2:
    Open client code in vscode and run `ember serve` in terminal to serve the client.
## Step 3:
    Open server code in intellij and click run configuration of TheaApplication to run the server.

# Run Thea from pre built jar
Pre-req to run the app from jar
- JRE-8
Note: will look into bundling jre and make this a standalone app.

# Steps for Running the App
download the jar from release and run the below command<br />
`java -jar thea-alpha-v1.jar`

# Disclaimer
This repo is currently only read only for public.<br />
Won't be accepting any open source contributors for now.<br /> 
Although anyone is free to use the codes and have their own branches.<br />

# Credits
- Logos By [Deenu Akash](https://www.linkedin.com/in/deenu-akash/)
- Other Libs:
    1. [UI Template from bootstrapdashteam](https://github.com/BootstrapDash/corona-free-dark-bootstrap-admin-template)
    2. [Pretty CheckBox](https://lokesh-coder.github.io/pretty-checkbox/)
    3. [Pretty Print Json](https://pretty-print-json.js.org/)
    4. [Material Design](https://materialdesignicons.com/)
    5. [Ember Material Design Icons](https://kaermorchen.github.io/ember-mdi/)
    6. [embedded-redis](https://github.com/kstyrc/embedded-redis)
